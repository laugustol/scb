import React, {Component} from 'react';
import {Alert,TouchableOpacity} from 'react-native';
import { Container } from 'native-base';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import {resetNavigation} from '../actions/nav';
import style from '../style';
import * as userActions from '../actions/user';
import RegisterView from '../Components/Register';
import DropdownAlert from 'react-native-dropdownalert';
import validate from '../utils/validation';
import I18n from 'react-native-i18n';
class Register extends Component{
    constructor(props)
    {
        super();
        this.state = {
            email: '',
            password: '',
            rpassword: '',
            lastName: '',
            name:'',
            countryData:{},
            phone:'',
            countryModal:false,
            emailError:null,
            passwordError:null,
            rpasswordError:null,
            lastNameError:null,
            nameError:null,
            countryDataError:null,
            phoneError:null
        }
        this.register = this.register.bind(this)
        this.changeText = this.changeText.bind(this)
        this.handlerContryModal = this.handlerContryModal.bind(this)
        this.validateForm = this.validateForm.bind(this)
    }
    changeText(key,value){
        switch (key) {
            case 'name':
                this.setState({...this.state,name:value})
                break;
            case 'lastName':
                this.setState({...this.state,lastName:value})
                break;
            case 'email':
                this.setState({...this.state,email:this.textoMinuscula(value)})
                break;
            case 'password':
                this.setState({...this.state,password:value})
                break;
            case 'rpassword':
                this.setState({...this.state,rpassword:value})
                break;
            case 'phone':
                this.setState({...this.state,phone:value})
                break;
            case 'countryData':
                this.setState({...this.state,countryData:{name:value.name,code:value.code},phone:value.code})
                break;
            default:
                break;
        }
    }
    openAlert(type,title,message){
        this.dropdown.alertWithType(type,title,message)
     }
    handlerContryModal(){
        this.setState({...this.state,countryModal: !this.state.countryModal})
    }
    register()
    {
        if(!this.state.nameError && !this.state.lastNameError && !this.state.emailError && !this.state.passwordError && !this.state.rpasswordError && !this.state.phoneError && !this.state.countryDataError){
            this.props.dispatch(userActions.register(this.state.name,this.state.lastName,this.state.email,this.state.password,this.state.rpassword))
                        .then(() => this.props.navigation.dispatch(NavigationActions.reset(resetNavigation('Home'))))
                        .catch(err => {this.props.navigation.dispatch(userActions.setFetching(false)); Alert.alert('Error',`${err}`,[{text: 'Aceptar'}])})
        }
    }
    validateForm(params)
    {
        switch (params) {
            case 'name':
            if(validate('min',this.state.name,{length:3})){this.setState({nameError:false})}else{this.setState({nameError:true}); this.openAlert('error','ERROR',I18n.t('validation_min_name_error'));}
            break;
            case 'lastname':
            if(validate('min',this.state.lastName,{length:3})){this.setState({lastNameError:false})}else{this.setState({lastNameError:true}); this.openAlert('error','ERROR',I18n.t('validation_min_lastname_error'));}
            break;
            case 'email':
            if(validate('email',this.state.email)){this.setState({emailError:false})}else{this.setState({emailError:true}); this.openAlert('error','ERROR',I18n.t('validation_email_error'));}
            break;
            case 'password':
            if(validate('password',this.state.password)){this.setState({passwordError:false})}else{this.setState({passwordError:true}); this.openAlert('error','ERROR',I18n.t('validation_password_min_error'));}
            break;
            case 'rpassword':
            if(validate('compare',{value1:this.state.password,value2:this.state.rpassword})){this.setState({rpasswordError:false})}else{this.setState({rpasswordError:true}); this.openAlert('error','ERROR',I18n.t('validation_repeat_password_error'));}
            break;
            case 'phone':
            if(validate('min',this.state.phone,{length:11})){this.setState({phoneError:false})}else{this.setState({phoneError:true}); this.openAlert('error','ERROR',I18n.t('validation_min_phone_error'));}
            break;
            case 'countryData':
            if(!validate('isEmpty',this.state.countryData)){this.setState({countryData:false})}else{this.setState({countryData:true}); this.openAlert('error','ERROR',I18n.t('validation_countrydata_error'));}
            default:
            break;
        }       
    }
    textoMinuscula(text)
    {
        return text.toLowerCase()
    }
    render(){
        return(
        <Container>
            <DropdownAlert
        ref={ref => this.dropdown = ref}
        containerStyle={{
          backgroundColor: 'red',
        }} />
            <RegisterView onValidate={this.validateForm} onRegister={this.register} changeText={this.changeText} {...this.state} handlerContryModal={this.handlerContryModal} />
        </Container>
        )
    }
}
export default connect()(Register);