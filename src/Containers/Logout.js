import React, {Component} from 'react';
import {connect} from 'react-redux'
import {NavigationActions} from 'react-navigation';
import {resetNavigation} from '../actions/nav';
import * as userActions from '../actions/user';

class Logout extends Component{
    componentDidMount(){
        this.props.dispatch(userActions.logout())
        this.props.dispatch(NavigationActions.reset(resetNavigation('Login')))
    }
    render(){
        return(null)
    }
}
export default connect()(Logout)