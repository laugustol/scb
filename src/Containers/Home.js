import React, {Component} from 'react';
//Componentes
import HomeView from '../Components/Home';
class Home extends Component{
  render(){
    return(
      <HomeView navigation={this.props.navigation} />
    )
  }
}
export default Home;