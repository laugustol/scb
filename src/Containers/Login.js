import React,{Component} from 'react';
import {connect} from 'react-redux';
import {Alert} from 'react-native';
import {NavigationActions} from 'react-navigation';
import * as userActions from '../actions/user';
import {resetNavigation} from '../actions/nav';
import {setLanguage} from '../actions/languages';
import validate from '../utils/validation';
import { Container } from 'native-base';
import I18n from 'react-native-i18n';
//Component
import LoginView from '../Components/Login'
import DropdownAlert from 'react-native-dropdownalert';
class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameError:null,
            passwordError:null,
        }
        this.changeText = this.changeText.bind(this)
        this.login = this.login.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.openAlert = this.openAlert.bind(this)
    }
    cambiar = (lenguage) => {
        this.props.dispatch(setLanguage(lenguage))
    }
    register = () => {
        this.props.navigation.dispatch({type:'Register'})
    }
    validateForm = (param) => {
        switch (param) {
            case 'username':
            if(validate('email',this.state.username)){this.setState({usernameError:false})}else{this.setState({usernameError:true}); this.openAlert('error','ERROR',I18n.t('validation_email_error'));}
            break;
            case 'password':
            if(validate('min',this.state.password,{length:8})){this.setState({passwordError:false})}else{this.setState({passwordError:true}); this.openAlert('error','ERROR',I18n.t('validation_password_min_error'))}
            break;
            default:
                break;
        }
        
    }

    login = () => {
        if(!this.state.passwordError && !this.state.usernameError)
        {
            this.props.dispatch(userActions.login(this.state.username,this.state.password))
            .then(() => this.props.navigation.dispatch(NavigationActions.reset(resetNavigation('Home'))))
            .catch(err => {
                this.props.navigation.dispatch(userActions.setFetching(false));
                Alert.alert('Error',`${err}`,[{text: 'Aceptar'}]);
            })
        }  
    }
    openAlert(type,title,message){
       this.dropdown.alertWithType(type,title,message)
    }
    changeText(key,value){
        switch (key) {
            case 'username':
                this.setState({...this.state,username:value})
                break
            case 'password':
                this.setState({...this.state,password:value})
                break
            default:
                break
        }
    }
    render(){
       return(
           <Container>
        <DropdownAlert
        ref={ref => this.dropdown = ref}
        containerStyle={{
          backgroundColor: 'red',
        }}
/>
<LoginView onValidate={this.validateForm} onLogin={this.login} onRegister={this.register} onCambiar={this.cambiar} changeText={this.changeText} {...this.state} />        
</Container>
        )
    }
}
export default connect()(Login);