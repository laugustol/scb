import React from 'react';
import {TouchableOpacity,Modal} from 'react-native'
import { Container, Content, Form, Item, Input, Label, Button} from 'native-base';
import {Row, Grid} from "react-native-easy-grid";
import style from '../style';
import CountryCodeList from 'react-native-country-code-list'
import TranslateText from '../i18n/TranslateText';
import i18n from 'react-native-i18n';
const RegisterView =(props) => {
    return(
    <Container style={style.container}>
        <Content padder>
            <Grid>
                <Row style={style.contentCenter}>
                    <TranslateText style={[style.H1,style.textWhite]} translationKey="register_form_title" />
                </Row>
                <Form style={{marginBottom:20,marginTop:20}} >
                {
                    props.nameError ?
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_name" style={style.textRed} /></Label>
                        <Input onBlur={()=> props.onValidate('name')} value={props.name} onChangeText={(value) => props.changeText('name',value)} style={style.textWhite} />
                    </Item>
                    :
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_name" style={style.textWhite} /></Label>
                        <Input onBlur={()=> props.onValidate('name')} style={style.textWhite} value={props.name} onChangeText={(value) => props.changeText('name',value)} />
                    </Item>
                }
                {
                    props.lastNameError ?
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_last_name" style={style.textRed} /></Label>
                        <Input onBlur={()=> props.onValidate('lastname')} style={style.textWhite} value={props.lastName} onChangeText={(value) => props.changeText('lastName',value)} />
                    </Item>
                    :
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_last_name" style={style.textWhite} /></Label>
                        <Input onBlur={()=> props.onValidate('lastname')} style={style.textWhite} value={props.lastName} onChangeText={(value) => props.changeText('lastName',value)} />
                    </Item>
                }
                {
                    props.emailError ?
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_email" style={style.textRed} /></Label>
                        <Input onBlur={()=> props.onValidate('email')} style={style.textWhite} keyboardType="email-address" value={props.email} onChangeText={(value) => props.changeText('email',value)} />
                    </Item>
                    :
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_email" style={style.textWhite} /></Label>
                        <Input onBlur={()=> props.onValidate('email')} style={style.textWhite} keyboardType="email-address" value={props.email} onChangeText={(value) => props.changeText('email',value)} />
                    </Item>
                }
                {
                     props.passwordError ?
                     <Item stackedLabel>
                         <Label><TranslateText translationKey="register_password" style={style.textRed} /></Label>
                         <Input onBlur={()=> props.onValidate('password')} style={style.textWhite} placeholderTextColor="white" placeholder={i18n.t('register_password_placeholder')} secureTextEntry={true} value={props.password} onChangeText={(value) => props.changeText('password',value)} />
                     </Item>
                     :
                     <Item stackedLabel>
                         <Label><TranslateText translationKey="register_password" style={style.textWhite} /></Label>
                         <Input onBlur={()=> props.onValidate('password')} style={style.textWhite} placeholderTextColor="white" placeholder={i18n.t('register_password_placeholder')} secureTextEntry={true} value={props.password} onChangeText={(value) => props.changeText('password',value)} />
                     </Item>
                }
                {
                    props.rpasswordError ?
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_confirm_password" style={style.textRed} /></Label>
                        <Input onBlur={()=> props.onValidate('rpassword')} style={style.textWhite} placeholderTextColor="white" placeholderTextColor="white" placeholder={i18n.t('register_password_placeholder')} secureTextEntry={true} value={props.rpassword} onChangeText={(value) => props.changeText('rpassword',value)} />
                    </Item>
                    :
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_confirm_password" style={style.textWhite} /></Label>
                        <Input onBlur={()=> props.onValidate('rpassword')} style={style.textWhite} placeholderTextColor="white" placeholder={i18n.t('register_password_placeholder')} secureTextEntry={true} value={props.rpassword} onChangeText={(value) => props.changeText('rpassword',value)} />
                    </Item>
                }
                <Item stackedLabel>
                    <Label><TranslateText translationKey="register_country" style={style.textWhite} /></Label>
                    <Input onBlur={()=> props.onValidate('countryData')} style={style.textWhite} value={props.countryData.name} onFocus={()=>{props.handlerContryModal()}} />
                </Item>
                {
                    props.phoneError ?
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_phone" style={style.textRed} /></Label>
                        <Input onBlur={()=> props.onValidate('phone')} maxLength={15} style={style.textWhite} keyboardType="phone-pad" value={props.phone} onChangeText={(value) => props.changeText('phone',value)}  />
                    </Item>
                    :
                    <Item stackedLabel>
                        <Label><TranslateText translationKey="register_phone" style={style.textWhite} /></Label>
                        <Input onBlur={()=> props.onValidate('phone')} maxLength={15} style={style.textWhite} keyboardType="phone-pad" value={props.phone} onChangeText={(value) => props.changeText('phone',value)}  />                   
                    </Item>
                }
                {
                    props.name && props.email && props.lastName && props.password && props.rpassword && props.phone && props.countryData ? 
                    <Button block onPress={() => props.onRegister()} style={{marginTop:20,marginRight:20,marginLeft:20}}><TranslateText translationKey="register_btn" style={style.textWhite} /></Button>
                    :<Button disabled block onPress={() => props.onRegister()} style={{marginTop:20,marginRight:20,marginLeft:20}}><TranslateText translationKey="register_btn" style={style.textWhite} /></Button>
                }                
</Form>
                <Row style={style.contentCenter}>
                    <TouchableOpacity >
                        <TranslateText translationKey="register_term_condition" style={[{fontWeight:'bold'},style.textWhite]} />
                    </TouchableOpacity>
                </Row>
            </Grid>
        </Content>
        <Modal
    animationType="slide"
    transparent={false}
    visible={props.countryModal}
    onRequestClose={() => {
        props.handlerContryModal()
    }}>
    <CountryCodeList
      onClickCell={(cellObject) => {
        setTimeout(()=>{
            props.changeText('countryData',cellObject)
        },500)
        props.handlerContryModal()
      }}
	  />
    </Modal>
    </Container>
    )
}

export default RegisterView;