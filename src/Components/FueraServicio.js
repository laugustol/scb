import React from 'react';
import { H1 } from 'native-base';

function fueraServicio(){
    return(
        <H1>Servicio temporalmente no disponible</H1>
    )
}
export default fueraServicio