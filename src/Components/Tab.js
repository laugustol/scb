import React from 'react';
import styles from '../style';
import {Image,TouchableOpacity,Text} from 'react-native';
import { H1, View} from 'native-base';
import { Col,Grid } from 'react-native-easy-grid';

function Tab(props){
    return(
        <TouchableOpacity style={styles.tabButton}>
            <Grid style={styles.tabItemsContainer}>
                <Col style={{width:60}}>
                <Image source={{uri:props.img}} style={styles.tabImage} />
                </Col>
                <Col>
                <H1 style={[styles.textGray]}>{`$ ${props.price}`}</H1>
                </Col>
            </Grid>
        </TouchableOpacity>
    )
}
export default Tab;