import React from 'react';
import {Container} from 'native-base';
import {ActivityIndicator, Image} from 'react-native';
import TranslateText from '../i18n/TranslateText';
//Estilos
import styles from '../style';

function Loading(props){
    return(
        <Container style={[styles.container,styles.loadingContainer]}>
            <Image style={styles.logo} source={require('../assets/img/logo.png')} />
            <ActivityIndicator color="white" size="large" />
            <TranslateText translationKey="loading_text" style={[styles.textWhite,styles.loadingText]} />
        </Container>
    )
}
export default Loading;