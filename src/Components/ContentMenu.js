import React from 'react';
import {Content,Header,Body, Container} from 'native-base';
import {DrawerItems} from 'react-navigation';

function ContentMenuView(props){
    return (
    <Container>
        <Content>
            <DrawerItems {...props} />
            
        </Content>
    </Container>)
}
export default ContentMenuView;