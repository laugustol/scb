import React from 'react';
import { Container, Content,Text} from 'native-base';
import {TouchableHighlight} from 'react-native';
import {Grid,Row} from 'react-native-easy-grid';
import styles from '../style';
import TranslateText from '../i18n/TranslateText';
//Componentes
import HeaderView from './Header';
import Tab from './Tab';

function HomeView(props){
    return(
        <Container style={styles.container} >
            <HeaderView navigation={props.navigation} />
            <Content padder>
                <Grid>
                    <Row style={styles.contentCenter} >
                        <Text style={[styles.H1,styles.textWhite]}>$ 2.000</Text>
                    </Row>
                    <Row style={styles.contentCenter}>
                        <TouchableHighlight>
                            <TranslateText translationKey="see_my_wallets" style={[styles.textGray,styles.H3]} />
                        </TouchableHighlight>
                    </Row>
                    <Row style={[styles.contentCenter,styles.tabContainer]}>
                        <Tab price='8.696,88' img='http://icons.iconarchive.com/icons/papirus-team/papirus-apps/48/bitcoin-icon.png' />
                        <Tab price='531,34' img='https://cdn4.iconfinder.com/data/icons/cryptocoins/227/ETH-48.png' />
                        <Tab price='50' img='http://icons.iconarchive.com/icons/cjdowner/cryptocurrency/48/Monero-icon.png' />
                        <Tab price='1' img='http://icons.iconarchive.com/icons/cjdowner/cryptocurrency/48/Zcash-icon.png' />
                    </Row>
                </Grid>
            </Content>
        </Container>
    )
}
export default HomeView;