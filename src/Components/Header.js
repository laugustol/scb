import React from 'react'
import {Button, Header, Left,Body} from 'native-base'
import {TouchableHighlight} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from '../style'

function HeaderView(props){
    return(
           <Header noShadow style={styles.headerContainer} >
               <Body>
                    <TouchableHighlight onPress={ () => props.navigation.navigate('DrawerOpen')}>
                        <Icon name='align-justify' style={styles.headerIcon} size={30} />
                    </TouchableHighlight>
                </Body>
            </Header>
    )
}
export default HeaderView;