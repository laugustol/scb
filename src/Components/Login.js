import React, {Component} from 'react';
import ConfigAPP from '../config';
import {TouchableHighlight,TouchableOpacity,Image} from 'react-native';
import { Container,Content,Button,Title,Form,Item,Input,Label,Text,Icon} from 'native-base';
import {Row, Grid} from "react-native-easy-grid";
import style from '../style';
import TranslateText from '../i18n/TranslateText';
const LoginView = (props) => {
    return (
        <Container style={style.container} >
           <Content padder>
               <Grid>
                   <Row style={style.contentCenter} >
                        <Title style={style.title}>
                           <Image style={style.logo} source={require('../assets/img/logo.png')} />
                        </Title>
                    </Row>
                    <Row style={style.contentCenter}>
                        <Text style={style.nameAPP}>{ConfigAPP.name}</Text>
                    </Row>
                        <Form floatingLabel style={{marginBottom:20,marginTop:20}} >
                        {
                            props.usernameError ?
                            <Item floatingLabel style={style.loginInput} >
                                <Label><TranslateText translationKey="login_input_email" style={style.textRed} /></Label>
                                <Input onBlur={()=> props.onValidate('username')} keyboardType="email-address" placeholderTextColor="white" style={style.textWhite} value={props.username} onChangeText={(text) => props.changeText('username',text)} />
                                <Icon style={style.textRed} name='close-circle' />
                            </Item>
                            :
                            <Item floatingLabel style={style.loginInput} >
                                <Label><TranslateText translationKey="login_input_email" style={style.textWhite} /></Label>
                                <Input onBlur={()=> props.onValidate('username')} keyboardType="email-address" placeholderTextColor="white" style={style.textWhite} value={props.username} onChangeText={(text) => props.changeText('username',text)} />
                            </Item>
                        }
                        {
                            props.passwordError ? 
                            <Item floatingLabel style={style.loginInput} >
                                <Label><TranslateText translationKey="login_input_password" style={style.textRed} /></Label>
                                <Input onBlur={()=> props.onValidate('password')} placeholderTextColor="white" style={style.textWhite} secureTextEntry={true} value={props.password} onChangeText={(text) => props.changeText('password',text)} />
                                <Icon style={style.textRed} name='close-circle' />

                            </Item>
                            :
                            <Item floatingLabel style={style.loginInput} >
                                <Label><TranslateText translationKey="login_input_password" style={style.textWhite} /></Label>
                                <Input onBlur={()=> props.onValidate('password')} placeholderTextColor="white" style={style.textWhite} secureTextEntry={true} value={props.password} onChangeText={(text) => props.changeText('password',text)} />
                            </Item>
                        }
                        </Form>
                    <Row style={style.contentCenter}>
                        <TouchableOpacity>
                         {props.username && props.password ? <Button rounded light style={style.loginBTN} onPress={() => props.onLogin()}><TranslateText translationKey="login_btn" /></Button> : <Button disabled rounded light style={style.loginBTN} onPress={() => props.onLogin()}><TranslateText translationKey="login_btn" /></Button>}
                        </TouchableOpacity>
                    </Row>
                    <Row style={style.contentCenter}>
                        <TouchableHighlight>
                            <TranslateText translationKey="forgot_password" style={[style.loginText,style.textWhite]} />
                        </TouchableHighlight>
                    </Row>
                    <Row style={style.contentCenter}>
                        <TouchableHighlight onPress={() => props.onRegister()} >
                            <TranslateText translationKey="create_new_account" style={style.textWhite} />
                        </TouchableHighlight>
                    </Row>
                    <Row style={style.contentCenter}>
                        <TouchableHighlight onPress={() => props.onCambiar('es')} >
                            <Image source={{uri:"https://cdn2.iconfinder.com/data/icons/flags_gosquared/32/Spain_flat.png"}} style={{height:32,width:32}} />
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => props.onCambiar('en')} >
                            <Image source={{uri:"https://cdn2.iconfinder.com/data/icons/flags_gosquared/32/United-Kingdom_flat.png"}} style={{height:32,width:32}} />
                        </TouchableHighlight>
                    </Row>
                </Grid>
            </Content>
      </Container>
    )
}
export default LoginView;