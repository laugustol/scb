import {Navigation} from '../navigator/NavigationConfig';
import {NavigationActions} from 'react-navigation'

export function navState(state,action){
    let newState;
    switch(action.type){
        case 'Register':
            newState = Navigation.router.getStateForAction(NavigationActions.navigate({routeName:'Register'}),state)
            break;
        case 'Home':
            newState = Navigation.router.getStateForAction(NavigationActions.navigate({routeName:'Home'}),state)
            break;
        default:
        newState = Navigation.router.getStateForAction(action,state)
        break;
    }
    return newState || state;
}