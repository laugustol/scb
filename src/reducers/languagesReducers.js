import {actionTypeLanguages} from '../actions/action-types';
import I18n from 'react-native-i18n';
const initialState = {
    language: I18n.defaultLocale
}
export function languagesState(state = initialState,action){
    switch(action.type){
        case actionTypeLanguages.SET_LANGUAGE:
            I18n.locale = action.payload
            return Object.assign({},state,{language:action.payload})
            break;
        default:
            return state;
            break;
    }
}

