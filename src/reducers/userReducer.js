//Actions Types
import userActions from "../actions/user";

const initialState = {
    isFetching:false,
    isAutenticate: false,
    token:'',
    failure:false,
    errorMessage:'',
    user:{}
}
export function userState(state=initialState,action){
    switch (action.type) {
        case 'SET_REQUEST':
            return Object.assign({},state,{isFetching:action.payload})
            break;
        case 'LOGIN_SUCCESS':
            return Object.assign({},state,{isFetching:false,isAutenticate:true,user:action.payload,failure:false})
            break;
        case 'LOGOUT':
            return Object.assign({},state,{isFetching:false,isAutenticate:false,user:{},failure:false})
            break;
        case 'LOGIN_FAILED':
            return Object.assign({},state,{isFetching:false,isAutenticate:false,failure:true,errorMessage:action.payload}) 
            break;
        default:
            return state;
            break;
    }
}