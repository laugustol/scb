import {combineReducers} from 'redux';
import {userState} from './userReducer';
import {navState} from './navigationReducer';
import {languagesState} from './languagesReducers';
const AppReducer = combineReducers({
  userState,
  navState,
  languagesState
})

export default AppReducer;