//Action Types
import {actionTypeUser} from "./action-types";

//Service
import {userService} from '../services/user';

export function setFetching(payload){
    return {
        type:actionTypeUser.SET_REQUEST,
        payload
    }
}
function LOGIN_SUCCESS(payload){
    return {
        type:actionTypeUser.LOGIN_SUCCESS,
        payload
    }
}
function LOGIN_FAILED(payload){
    return {
        type:actionTypeUser.LOGIN_FAILED,
        payload
    }
}
function USER_LOGOUT(){
    return {
        type:actionTypeUser.LOGOUT
    }
}

export function login(email,password){
    return dispatch => {
        dispatch(setFetching(true))
        return userService.login(email,password)
        .then(result => {
            if(result.ok){
                dispatch(LOGIN_SUCCESS({email:result.user.email,name:result.user.name}))
                return Promise.resolve();
            }
            else{
                return Promise.reject(result.message)
            }
        })
    }
}
export function register(name,lastname,email,password){
    return dispatch => {
        dispatch(setFetching(true))
        return userService.register(name,lastname,email,password)
        .then(result => {
            if(result.ok){
                dispatch(LOGIN_SUCCESS({email:result.user.email,name:result.user.name}))
                return Promise.resolve();
            }else{
                return Promise.reject(err.message)
            }
        })
    }
}
export function logout(){
    return dispatch => {
        dispatch(USER_LOGOUT())
    }
}