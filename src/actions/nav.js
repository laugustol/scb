import {actionTypeRoutes} from './action-types';
import {NavigationActions} from 'react-navigation';

export function resetNavigation(route){
    return {
        index:0,
        actions:[NavigationActions.navigate({routeName:route})]
    }
}
export function home(){
    return {
        routeName:actionTypeRoutes.Home
    }
}
export function register(){
    return {
        routeName:actionTypeRoutes.Register
    }
}
export function login(){
    return {
        routeName:actionTypeRoutes.Login
    }
}