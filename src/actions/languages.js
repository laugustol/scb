import {actionTypeLanguages} from './action-types';

export function setLanguage(language){
    return{
        type:actionTypeLanguages.SET_LANGUAGE,
        payload:language
    }
}