/*Acciones de rutas */
export const actionTypeRoutes = {
    Login: 'Login',
    Register: 'Register',
    Home: 'Home',
};
export const actionTypeUser = {
    SET_REQUEST: 'SET_REQUEST',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILED: 'LOGIN_FAILED',
    LOGOUT:'LOGOUT'
};
export const actionTypeLanguages = {
    SET_LANGUAGE: 'SET_LANGUAGE'
};