import I18n from 'react-native-i18n'
import ConfigAPP from '../config';
import translations from './translations/index';

I18n.defaultLocale = ConfigAPP.language_default;
I18n.locale = ConfigAPP.language_default;
I18n.fallbacks = true;
I18n.translations = translations;
export default I18n;