import React, {Component} from 'react';
import {Text,Alert} from 'react-native';
import {connect} from 'react-redux';
import I18n from 'react-native-i18n';

class TranslateText extends Component{
    render(){
        const {translationKey,style} = this.props
        if(style){
            return(<Text style={style}>{I18n.t(translationKey)}</Text>)
        }else{
            return(<Text>{I18n.t(translationKey)}</Text>)
        }
    }
}
function mapStateToProps(state){
    return{
        language: state.languagesState.language
    }
}
export default connect(mapStateToProps)(TranslateText);