import TranslateText from '../i18n/TranslateText';
function validate(type,value,options={}){
	var result;
	switch (type) {
		case 'email':
			var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
			reg.test(value) ? result = true : result = false
			return result
			break;
		case 'password':
			var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,20}$/
			reg.test(value) ? result = true : result = false
			return result
			break;
		case 'min':
			value.length >= options.length ? result = true : result = false
			return result
			break;
		case 'max':
			value.length <= options.length ? result = true : result = false
			return result
			break;
		case 'isEmpty':
			value == 0 ? result = true : result = false
			return result
			break;
		case 'compare':
			value.value1 == value.value2 ? result = true : result = false
			return result
			break;
		default:
			break;
	}
}

export default validate;