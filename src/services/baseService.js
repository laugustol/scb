const baseURL = 'http://52.14.61.194:4444';

function createHeaders(){
    return{'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json' }
}

function consultarGet(url){
   let headers = createHeaders();
   return fetch(baseURL + url,{method:'GET',headers})
   .then(resolve => {return resolve.json()})
   .catch(err => {return {error:true,msj:`Error: Fallo de conexion.`}})
}

function consultarPost(url,data){
    let headers = createHeaders();
    return fetch(baseURL + url,{method:'POST',headers,body: JSON.stringify(data)})
    .then(resolve => resolve.json())
    .catch(err => {return {ok:true,msj:`Error: Fallo de conexion.`}})
}

export default baseService = {
    consultarGet,
    consultarPost
};