import baseService from "./baseService";

function login(email,password){
    return baseService.consultarPost('/login',{email,password})
    .then(resolve => {
        if(!resolve.ok){
            return Promise.reject(resolve.message)
        }
        return resolve
    })
}
function register(name,lastname,email,password){
    return baseService.consultarPost('/signup',{name,lastname,email,password})
    .then(resolve => {
        if(!resolve.ok){
            return Promise.reject(resolve.message)
        }
        return resolve
    })
}
function logout(email){
    return baseService.consultarGet('/logout',{email})
    .then(resolve => {
        if(!resolve.ok){
            return Promise.reject(resolve.message)
        }
        return resolve
    })
}
export const userService = {
    login,
    register,
    logout
}