import React, {Component} from 'react';
import {View,Text} from 'react-native';
import {createStore,applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import AppReducer from './reducers/index';
import {middleware} from './utils/middleware-redux';
import thunk from 'redux-thunk';
import Navigator from './navigator/index';
import I18n from './i18n/index';
const store = createStore(AppReducer,applyMiddleware(thunk,middleware));


class App extends Component{
    render(){
        return(
            <Provider store={store}>
                <Navigator />
            </Provider>
        )
    }
}
export default App;