import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    //GLOBAL STYLE
    H1:{
        fontSize: 2 * (window.BASE_FONT_SIZE || 18)
    },
    H2:{
        fontSize: 1.5 * (window.BASE_FONT_SIZE || 18)
    },
    H3:{
        fontSize: 1.17 * (window.BASE_FONT_SIZE || 18)
    },
    H4:{
        fontSize: 1 * (window.BASE_FONT_SIZE || 18)
    },
    H5:{
        fontSize:.83 * (window.BASE_FONT_SIZE || 18)
    },
    H6:{
        fontSize:.67 * (window.BASE_FONT_SIZE || 18)
    },
    container:{
        backgroundColor:'#213663',
    },
    contentCenter:{
        justifyContent:'center',
        marginTop:5
    },
    title:{
        textShadowOffset:{
            width:3,
            height:3
        },
        fontSize:50,
        color:'#fff',
        paddingTop:30,
        paddingBottom:20,
    },
    textGray:{
        color:'#7e7e7e'
    },
    textWhite:{
        color:'white'
    },
    textRed:{
        color:'red'
    },
    logo:{
        height:120,
        width:120
    },
    //Header STYLES
    headerContainer:{
        backgroundColor:'transparent'
    },
    headerIcon:{
        color:'white'
    },
    //LOGIN STYLES
    nameAPP:{
        color:'#fff',
        fontSize:20
    },
    loginBTN:{
        padding:20,
    },
    loginTextBTN:{
        color:'#5c5c5c'
    },
    loginText:{
        marginTop:20
    },
    loginInput:{
        borderBottomColor:'#012b67'
    },
    //HOME STYLES
    //Tab Style
    tabContainer:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        marginTop:10
    },
    tabButton:{
        backgroundColor:'white',
        borderRadius:10,
        marginBottom:10,
        width:230
    },
    tabItemsContainer:{
        flexDirection:'row',
        alignItems:'center',
        padding:5,
    },
    tabText:{
        textAlign:'left',
    },
    tabImage:{
        width:48,
        height:48,
    },
    //Loading styles
    loadingContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loadingText:{
        fontSize:20
    },
});