import React from 'react';
import {DrawerNavigator,StackNavigator} from 'react-navigation';

import Login from '../Containers/Login'
import Logout from '../Containers/Logout'
import Register from '../Containers/Register'
import Home from '../Containers/Home'
import FueraServicio from '../Components/FueraServicio'
import ContentMenuView from '../Components/ContentMenu'

const UserNavigation = DrawerNavigator({
  Home: { screen: Home },
  Wallet: {screen: FueraServicio},
  Transferencias: {screen: FueraServicio},
  Perfil: {screen: FueraServicio},
  CrytoPlus: {screen: FueraServicio},
  CryptoFijo: {screen: FueraServicio},
  CryptoServicios: {screen: FueraServicio},
  Solicitudes: {screen: FueraServicio},
  Productos: {screen: FueraServicio},
  Consignaciones: {screen: FueraServicio},
  Alertas: {screen: FueraServicio},
  Salir: {screen: Logout},
},{
  initialRouteName:'Home',
  contentComponent: ContentMenuView
});

export const Navigation = StackNavigator({
  Login:{screen:Login},
  Register:{screen:Register},
  Home:{screen:UserNavigation}
},{headerMode:'none'});