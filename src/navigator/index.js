import React, {Component} from 'react';
import {connect} from 'react-redux';
import {BackHandler,Alert} from 'react-native';
import {addNavigationHelpers} from 'react-navigation';
import {Navigation} from './NavigationConfig';
import {NavigationActions} from 'react-navigation';
//Actions
import * as userActions from '../actions/user';
import {resetNavigation} from '../actions/nav';
//Components
import Loading from '../Components/Loading';
//Utils
import {addListener} from '../utils/middleware-redux';

class Navigator extends Component{
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
      }
      onLogout(){
        this.props.dispatch(userActions.logout())
        this.props.dispatch(NavigationActions.reset(resetNavigation('Login')))
      }
      onBackPress = () => {
        const { dispatch, navState } = this.props;
        if (navState.routes[0].routeName === 'Home' && navState.routes[0].routes[0].index === 0) {
            Alert.alert('Alert','Message',[{text:'Logout', onPress: () => this.onLogout()},{text:'Cancel'}])
            return true;
        }
        if(navState.routes[0].routeName === 'Login' && navState.index === 0) return false;
        dispatch(NavigationActions.back());
        return true;
      };
    render(){
        if(this.props.isFetching){
            return(<Loading />)
        }else{
            return(
                <Navigation 
                    navigation={addNavigationHelpers({
                        dispatch: this.props.dispatch,
                        state: this.props.navState,
                        addListener
                    })}
                />
            )
        }
    }
}
function mapStateToProps(state){
    return {
        navState: state.navState,
        isFetching: state.userState.isFetching
    }
}
export default connect(mapStateToProps)(Navigator);