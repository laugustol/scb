# Satoshi Crypto Bank APP

**Satoshi Crypto Bank** es una aplicación móvil creada para ofrecer el servicio de Wallet y Exchange mediante un celular inteligente con sistema operativo **Android** o **iOS**

## Instalación
* Clonar el repositorio
```bash
git clone https://gitlab.com/laugustol/scb.git
```

* Generar keystore. 
* Usaremos la herramienta **keytools** de android studio.
```bash
keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
```
* Se creara un archivo **.keystore**, debe ser movido a la carpeta android/app del repositorio.
* Editaremos el archivo **gradle.properties** que se encuentra en la ruta **~/.gradle/** y agregaremos el siguiente contenido. Remplazar los * por tu clave secreta que ingresaste en el proceso de creación de la keystore.
```bash
MYAPP_RELEASE_STORE_FILE=my-release-key.keystore MYAPP_RELEASE_KEY_ALIAS=my-key-alias MYAPP_RELEASE_STORE_PASSWORD=***** MYAPP_RELEASE_KEY_PASSWORD=*****
```
* Editaremos el archivo **build.gradle** que se encuentra en la ruta **android/app** del repositorio y agregaremos el siguiente contenido
```JSON
signingConfigs 
{ 
    release 
    { 
        storeFile file(MYAPP_RELEASE_STORE_FILE)
        storePassword MYAPP_RELEASE_STORE_PASSWORD 
        keyAlias MYAPP_RELEASE_KEY_ALIAS 
        keyPassword MYAPP_RELEASE_KEY_PASSWORD 
    } 
} 
buildTypes 
{ 
    release 
    { 
        signingConfig signingConfigs.release 
    }
}
```
* Ejecutaremos la siguiente instrucción en nuestra terminal (Ubicaremos la consola en la carpeta del repositorio)
```bash
cd android && ./gradlew assembleRelease
```

El **APK** se debe haber generado en la ruta: **android/app/build/outputs/apk**
